const express = require('express');
const router = express.Router({ mergeParams: true });
const Reservation = require('./reservation.model');
const Hotel = require('../hotel/hotel.model');

// Ensure that a reservation exists for the given ID before continuing.
// Add a reference to the found reservation into the request.
router.use('/:reservationId', (req, res, next) => {
  const reservationId = req.params.reservationId;
  Reservation.findById(reservationId)
    .populate('room')
    .populate('createdByUser')
    .exec()
    .then(foundReservation => {
      if (!foundReservation) {
        throw new Error('Specified reservation could not be found.');
      } else if (foundReservation.room._id.toString() !== req.params.roomId) {
        throw new Error('Reservation and room mismatch.');
      }
      foundReservation.createdByUser.password = null;
      req.reservation = foundReservation;
      next();
    })
    .catch(err => {
      err.status = 400;
      next(err);
    });
});

module.exports = router;