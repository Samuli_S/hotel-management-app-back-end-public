const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const addressSchema = require('../shared/schemas/address.schema');

const customerSchema = new Schema({
  firstName: {
    type: String,
    required: true
  },
  lastName: {
    type: String,
    required: true
  },
  phoneNumber: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  address: {
    type: addressSchema,
    required: true
  },
});

const reservationSchema = new Schema({
  room: {
    type: Schema.Types.ObjectId, ref: 'Room',
    required: true
  },
  createdByUser: {
    type: Schema.Types.ObjectId, ref: 'User',
    required: true
  },
  startDate: {
    type: Date,
  },
  endDate: {
    type: Date,
  },
  customer: {
    type: customerSchema,
    required: true
  }
});

module.exports = mongoose.model('Reservation', reservationSchema);