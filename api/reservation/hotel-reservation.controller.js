/* 
  A controller for hotel reservation request (.../hotels/:hotelId/reservations).
  The default reservation controller does not handle this route. This is because it
  is intended to be used with resoucre requests that are based on hotel and room 
  parameters.
*/

const express = require('express');
const router = express.Router({ mergeParams: true });
const Reservation = require('./reservation.model')
const Room = require('../room/room.model');

router.get('/', (req, res, next) => {
  // Find all reserved rooms for the specified hotel. 
  // Map an array of room IDs and use it to query reservations with matching 
  // room IDs.
  const hotelId = req.params.hotelId;
  Room.find({ hotel: hotelId }).exec()
    .then(rooms => {
      const roomIds = rooms.map(room => {
        return room._id;
      });
      return Reservation.find()
        .where('room')
        .in(roomIds)
        .exec();
    })
    .then(foundReservations => {
      res.status(200);
      return res.json(foundReservations);
    })
    .catch(err => {
      err.status = 500;
      next(err);
    });
});

module.exports = router;
