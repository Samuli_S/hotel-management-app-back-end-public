const express = require('express');
const router = express.Router({ mergeParams: true });
const mongoose = require('mongoose');
const Hotel = require('../hotel/hotel.model');
const Reservation = require('./reservation.model')
const Room = require('../room/room.model');

router.post('/', (req, res, next) => {
  const roomReservation = {
    room: req.params.roomId,
    createdByUser: req.auth.user._id,
    ...req.body
  };
  let createdReservation;

  Reservation.findOne({ room: req.room._id })
    .then(room => {
      if (room) {
        const error = new Error('Room is already reserved.');
        error.name = "Forbidden Action Error";
        throw error;
      }
      return Reservation.create(roomReservation);
    })
    .then(reservation => {
      createdReservation = reservation;
      req.room.set('isReserved', true);
      return req.room.save();
    })
    .then(() => {
      res.status(201);
      return res.json(createdReservation);
    })
    .catch(err => {
      err.status = 400;
      next(err);
    });
});

router.get('/', (req, res, next) => {
  // Find all rooms for the specified hotel. Map an array of room IDs and use
  // it to query reservations with matching room IDs.
  const hotelId = req.params.hotelId;
  Room.find({ hotel: hotelId }).exec()
    .then(rooms => {
      const roomIds = rooms.map(room => {
        return room._id;//mongoose.Types.ObjectId(room._id);
      });
      return Reservation.find()
        .where('room')
        .in(roomIds)
        .exec();
    })
    .then(foundReservations => {
      res.status(200);
      return res.json(foundReservations);
    })
    .catch(err => {
      err.status = 500;
      next(err);
    });
});

router.get('/:reservationId', (req, res, next) => {
  res.status(200);
  res.json(req.reservation);
});

router.patch('/:reservationId', (req, res, next) => {
  // Do not allow users to change the reservation creator.
  if (req.body.createdByUser) {
    delete req.body.createdByUser;
  }
  const currentReservation = req.reservation;
  const targetSubDocumentName = req.query.target;
  if (targetSubDocumentName) {
    // Reservation customer subdocument is being updated.
    const reqCustomer = req.body.customer;
    switch(targetSubDocumentName) {
      case 'customer':
        Object.assign(currentReservation.customer, reqCustomer);
        break;
      case 'customer-address':
        Object.assign(currentReservation.customer.address, reqCustomer.address);
        break;
      default:
          const error = new Error('Invalid query string parameter for target');
          next(error);
          return;
        break;
    }
  } else {
    // If no query string exists, then only the parent reservation document 
    // shall be updated.
    Object.assign(currentReservation, req.body);
  }
  currentReservation.save()
    .then(updatedReservation => {
      res.status(201);
      return res.json(updatedReservation);
    })
    .catch(err => {
      err.status = 400;
      next(err);
    });
});

router.delete('/:reservationId', (req, res, next) => {
  req.reservation.remove()
    .then(() => {
      res.status(204);
      return res.end();
    })
    .catch(err => {
      err.status = 400;
      next(err);
    });
});

module.exports = router;