const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const addressSchema = require('../shared/schemas/address.schema');

const hotelSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  address: {
    type: addressSchema,
    required: true
  }
});

module.exports = mongoose.model('Hotel', hotelSchema);