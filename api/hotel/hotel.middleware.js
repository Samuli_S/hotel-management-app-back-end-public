const express = require('express');
const router = express.Router();
const Hotel = require('./hotel.model');

// Ensure that a hotel exists for the given ID before continuing.
// Add a reference to the found hotel into the request.
router.use('/:hotelId', (req, res, next) => {
  const hotelId = req.params.hotelId;
  Hotel.findById(hotelId).exec()
    .then(foundHotel => {
      if (!foundHotel) {
        throw new Error('Specified hotel does not exist.');
      }
      req.hotel = foundHotel;
      next();
    })
    .catch(err => {
      err.status = 400;
      next(err);
    })
});

module.exports = router;