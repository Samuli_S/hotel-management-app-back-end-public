const express = require('express');
const router = express.Router();
const Hotel = require('./hotel.model');

router.post('/', (req, res, next) => {
  const hotel = req.body;
  Hotel.create(hotel)
    .then(createdHotel => {
      res.status(201);
      return res.json(createdHotel);
    })
    .catch(err => {
      err.status = 400;
      next(err);
    });
});

router.get('/', (req, res, next) => {
  Hotel.find()
    .then(foundHotels => {
      res.status(200);
      return res.json(foundHotels);
    })
    .catch(err => {
      err.status = 500;
      next(err);
    });
});

router.patch('/:hotelId', (req, res, next) => {
  const currentHotel = req.hotel;
  currentHotel.set(req.body);
  currentHotel.save()
    .then(updatedHotel => {
      res.status(201);
      return res.json(updatedHotel);
    })
    .catch(err => {
      err.status = 400;
      next(err);
    });
});

router.delete('/:hotelId', (req, res, next) => {
  const currentHotel = req.hotel;
  currentHotel.remove()
    .then(() => {
      res.status(204);
      return res.end();
    })
    .catch(err => {
      err.status = 400;
      next(err);
    });    
});

module.exports = router;