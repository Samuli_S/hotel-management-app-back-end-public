const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');

router.use((req, res, next) => {
  const authHeader = req.headers.authorization;
  if (!authHeader) {
    const error = new Error('Unauthorized');
    error.status = 401;
    next(error);
    return;
  }
  const bearer = authHeader.split(' ');
  const token = bearer[1];
  jwt.verify(token, 'superSecret_String', (err, decoded) => {
    if (err) {
      const error = new Error('Unauthorized');
      error.status = 401;
      next(error);
      return;
    }
    req.auth = decoded;
    next();
    return;
  });
})

module.exports = router;
