const express = require('express');
const router = express.Router();
const User = require('../../user/user.model');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

router.post('/', (req, res, next) => {
  const email = req.body.email;
  const password = req.body.password;
  if (!email || !password) {
    const error = new Error('Missing email and/or password');
    error.status = 400;
    next(error);
    return;
  }
  User.findOne({ email }).exec()
    .then(foundUser => {
      if (foundUser) {
        const error = new Error('Email address is in use.');
        error.status = 400;
        throw error;
      }
      return bcrypt.hash(req.body.password, 10);
    })
    .then(encrypted => {
      req.body.password = encrypted;
      return User.create(req.body);
    })
    .then(createdUser => {
      jwt.sign({ user: createdUser }, 'superSecret_String', (err, token) => {
        if (err) {
          err.status = 400;
          err.message = 'An authentication error occured.';
          throw err;
        }
        return res.status(200).json({
          token,
          user: {
            _id: createdUser._id,
            email: createdUser.email
          }
        });
      });
    })
    .catch(err => {
      err.status = err.status || 500;
      err.name = 'Authentication Error';
      next(err);
      return;
    });
})

module.exports = router;