const express = require('express');
const router = express.Router();
const User = require('../../user/user.model');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

router.post('/', (req, res, next) => {
  const email = req.body.email;
  const password = req.body.password;
  if (!email || !password) {
    const error = new Error('Missing email and/or password');
    error.status = 400;
    next(error);
    return;
  }

  let foundUser; // User with the valid email address.
  User.findOne({ email }).exec()
    .then(user => {
      if (!user) {
        throw new Error();
      }
      foundUser = user;
      return bcrypt.compare(password, foundUser.password);
    })
    .then(isUserAuthenticated => {
      if (!isUserAuthenticated) {
        throw new Error();
      }
      jwt.sign({ user: foundUser }, 'superSecret_String', (err, token) => {
        if (err) {
          throw new Error();
        }
        res.status(200).json({
          token,
          user: {
            _id: foundUser._id,
            email: foundUser.email
          }
        });
      });
    })
    .catch(err => {
      // Forward a generic error for authentication failure.
      err.status = 400;
      err.name = 'Authentication Error';
      err.message = "An authentication error occured.";
      next(err);
      return;
    });
})

module.exports = router;