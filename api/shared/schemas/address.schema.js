const mongoose = require('mongoose');
const Schema = mongoose.Schema;

module.exports = new Schema({
  street: {
    type: String,
    required: true
  },
  postalCode: {
    type: String,
    required: true
  },
  city: {
    type: String,
    required: true
  }
});