const express = require('express');
const router = express.Router();

// Remove critical resource data (e.g. IDs) from the request body so that
// resource IDs can not be modified by update (patch) requests.
router.use((req, res, next) => {
  if (req.body._id) {
    req.body._id = null;
  }
  if (req.body.__v) {
    req.body.__v = null;
  }
  next();
});

module.exports = router;