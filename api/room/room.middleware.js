const express = require('express');
const router = express.Router({ mergeParams: true });
const Room = require('./room.model');

// Ensure that a room exists for the given ID before continuing.
// Add a reference to the found room into the request.
router.use('/:roomId', (req, res, next) => {
  const roomId = req.params.roomId;
  Room.findById(roomId).exec()
    .then(foundRoom => {
      if (!foundRoom) {
        throw new Error('Specified room does not exist.');
      } else if (foundRoom.hotel.toString() !== 
                  req.hotel._id.toString()) {
        throw new Error('Room and hotel mismatch.');
      }
      req.room = foundRoom;
      next();
    })
    .catch(err => {
      err.status = 400;
      next(err);
    })
});

module.exports = router;