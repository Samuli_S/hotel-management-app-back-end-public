const express = require('express');
const router = express.Router({ mergeParams: true });
const Room = require('./room.model');
const Reservation = require('../reservation/reservation.model');
const Hotel = require('../hotel/hotel.controller');

router.post('/', (req, res, next) => {
  const room = {
    hotel: req.params.hotelId,
    ...req.body
  }
  Room.create(room)
    .then(createdRoom => {
      res.status(201);
      return res.json(createdRoom);
    })
    .catch(err => {
      err.status = 400;
      next(err);
    });
});

router.get('/', (req, res, next) => {
  const hotelId = req.params.hotelId;
  if (req.query.type === 'notReserved') {
    // Only get the not reserved rooms per query parameter.
    Room.find({ hotel: hotelId })
    .where({ isReserved: false })
    .then(rooms => {
      res.status(200)
      .json(rooms);
    })
    .catch(err => {
      console.log('2eeeee', err);
      err.status = 500;
      next(err);
    });
    // Returning in the above promise success function does not work.
    // Return must be placed here in order to avoid executing code below.
    return;
  }
  Room.find({ hotel: hotelId })
  .then(rooms => {
    res.status(200);
    res.json(rooms);
  })
  .catch(err => {
    err.status = 500;
    next(err);
  });
});

router.patch('/:roomId', (req, res, next) => {
  req.room.set(req.body);
  req.room.save()
    .then(updatedRoom => {
      res.status(201);
      return res.json(updatedRoom);
    })
    .catch(err => {
      err.status = 400;
      next(err);
    }); 
});

router.delete('/:roomId', (req, res, next) => {
  req.room.remove()
    .then(() => {
      res.status(204);
      return res.end();
    })
    .catch(err => {
      err.status = 400;
      next(err);
    });
});

module.exports = router;