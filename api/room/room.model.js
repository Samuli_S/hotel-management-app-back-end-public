const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const roomSchema = new Schema({
  hotel: {
    type: Schema.Types.ObjectId, ref: 'Hotel',
    required: true
  },
  number: {
    type: Number,
    required: true
  },
  floor: {
    type: Number,
    required: true
  },
  area: {
    type: Number,
    required: true
  },
  hasView: {
    type: Boolean,
    required: true
  },
  isVIP: {
    type: Boolean,
    required: true
  },
  isReserved: {
    type: Boolean,
    required: true
  }
});

module.exports = mongoose.model('Room', roomSchema);