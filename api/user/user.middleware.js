const express = require('express');
const router = express.Router({ mergeParams: true });
const User = require('./user.model');

// Ensure that the user exists for the given ID before continuing.
// Add a reference to the found user into the request.
router.use('/:userId', (req, res, next) => {
  const userId = req.params.userId;
  User.findById(userId).exec()
    .then(foundUser => {
      if (!foundUser) {
        throw new Error('Specified user does not exist.');
      }
      req.user = foundUser;
      next();
    })
    .catch(err => {
      err.status = 400;
      next(err);
    })
});

module.exports = router;