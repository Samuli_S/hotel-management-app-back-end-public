const express = require('express');
const router = express.Router({ mergeParams: true });
const User = require('./user.model');

router.get('/', (req, res, next) => {
  User.find().select('-password')
    .then(foundUsers => {
      res.status(200);
      return res.json(foundUsers);
    })
    .catch(err => {
      err.status = 500;
      next(err);
    });
});

router.get('/:userId', (req, res, next) => {
  const userId = req.params.userId;
  User.findById(userId).select('-password')
    .then(foundUser => {
      res.status(200);
      return res.json(foundUser);
    })
    .catch(err => {
      err.status = 400;
      next(err);
    });
});

router.patch('/:userId', (req, res, next) => {
  req.user.set(req.body);
  req.user.save()
    .then(updatedUser => {
      res.status(201);
      return res.json(updatedUser);
    })
    .catch(err => {
      err.status = 400;
      next(err);
    }); 
});

router.delete('/:userId', (req, res, next) => {
  req.user.remove()
    .then(() => {
      res.status(204);
      return res.end();
    })
    .catch(err => {
      err.status = 400;
      next(err);
    });
});

module.exports = router;