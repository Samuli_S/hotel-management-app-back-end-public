const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
  defaultHotel: {
    type: Schema.Types.ObjectId, ref: 'Hotel'
  },
  email: {
    type: String,
    required: true
  },
  // Passwords are temporarily in clear text for prototyping.
  password: {
    type: String
  }
});

module.exports = mongoose.model('User', userSchema);