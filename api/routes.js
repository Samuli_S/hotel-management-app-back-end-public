const express = require('express');
const router = express.Router();

const resourceMiddleware = require('./shared/middleware/resource.middleware');
const authMiddleware = require('./auth/auth.middleware');
const userMiddleware = require('./user/user.middleware');
const hotelMiddleware = require('./hotel/hotel.middleware');
const roomMiddleware = require('./room/room.middleware');
const reservationMiddleware = require('./reservation/reservation.middleware');

const signUpController = require('./auth/sign-up/sign-up.controller');
const signInController = require('./auth/sign-in/sign-in.controller');
const userController = require('./user/user.controller');
const hotelController = require('./hotel/hotel.controller');
const roomController = require('./room/room.controller');
const reservationController = require('./reservation/reservation.controller');
const hotelReservationController = require('./reservation/hotel-reservation.controller');

// Authentication middleware.
router.use('/users', authMiddleware);
router.use('/hotels', authMiddleware);

// Authentication controllers.
router.use('/auth/sign-up', signUpController);
router.use('/auth/sign-in', signInController);

// Feature middleware.
router.use('/users', userMiddleware);
router.use('/hotels', hotelMiddleware);
router.use('/hotels/:hotelId/rooms', roomMiddleware);
router.use('/hotels/:hotelId/rooms/:roomId/reservations', reservationMiddleware);

// Feature controllers.
router.use('/users', userController);
router.use('/hotels', hotelController);
router.use('/hotels/:hotelId/rooms', roomController);
router.use('/hotels/:hotelId/rooms/:roomId/reservations', reservationController);

router.use('/hotels/:hotelId/reservations', hotelReservationController);

// Main error handler.
router.use((err, req, res, next) => {
  //res.status(err.status || 500);
  return res.json({
    apiError: {
      name: err.name,
      message: err.message
    }
  });
});

module.exports = router;