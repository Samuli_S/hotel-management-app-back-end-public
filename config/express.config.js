const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const path = require('path');
const apiRoutes = require('../api/routes');

app.use(express.static(path.join(__dirname, '../public')));

app.use(bodyParser.json());
app.use('/api/v1', apiRoutes);

app.get('*', (req, res) => {
  res.sendFile(path.join( __dirname, '../public/index.html'));
});

module.exports = app;