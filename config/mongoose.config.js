const mongoose = require('mongoose');
const secrets = require('./secrets');

mongoose.Promise = global.Promise;

const databaseName = 'hotel-management-db';
// const db = mongoose.connection;
// db.on('connecting', () => {
//   console.log(`[Database]: connecting to <${databaseName}>...`);
// })
// .on('connected', () => {
//   console.log(`[Database]: connected to <${databaseName}>.`);
// })
// .on('error', (err) => {
//   console.log(`[Database]: connection error: <${err}>`);
// })
// .on('reconnected', () => {
//   console.log(`[Database]: reconnecting to <${databaseName}>...`);
// })
// .on('disconnected', () => {
//   console.log(`[Database]: disconnected <${databaseName}>.`);
// });

mongoose.connect(secrets.db.connectionString, {
  keepAlive: 120
})
.then(() => {
  console.log(`[Database]: connected`);
})
.catch(err => {
  console.log(`[Database]: connection error: <${err}>`);
});

module.exports = mongoose;