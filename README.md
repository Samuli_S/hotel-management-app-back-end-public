# README #

Hotel Management App (HMA) prototype back end.

### What does this repository contain? ###

* This repository stores Hotel Management App back end.
* The backend is implemented as an RESTful API that also serves a Angular 5 application for other requests.
* Authorization credentials (config/secrets.js) have been removed from this codebase.
* The structure of the rest api is as follows: <domain>/api/v1/hotels/:hotelId/rooms/:roomId/reservations/:reservationId
* Based on this structure, it can be seen that hotels are the base resources that contain rooms, which in turn have
  reservations. The controlling API routes situated in api/routes.js shed some light on how this app operates.
* json web token (jwt) authentication have been implemented for all API routes except sign in and sign up.
* A feature-based architecture (use of e.g. user and auth folders) has been implemented to make the app more manageable 
  and maintainable.
* Express middleware handlers are a central part of this app for authentication and looking up resources before the
  requests are handled in their final handlers. This was done to manage the complexity of these handlers.
* MongoDB is accessed through Mongoose and the Mongoose models are used as request validators (forced required fields).
* The MongoDB instace is hosted at mLab.

### How do I get set up? ###

* Clone this repository.
* Run at app root > NPM install
* Add your own config/secrets.js file that exports the object: { db: { connectionString: <your mongodb connection string> } }
* This file is already included in config/mongoose.config.js and so the connection string will be used thereafter.
* Run at app root > node server.js

### Ownership ###

* Repository owner: Samuli Siitonen